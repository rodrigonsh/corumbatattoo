var langs = ['pt', 'en']
var langIdx = 0;

window.fn = {};

window.fn.open = function() {
  var menu = document.getElementById('menu');
  menu.toggle();
};

window.fn.load = function(page) {
  var content = document.getElementById('content');
  var menu = document.getElementById('menu');
  content.load(page)
    .then(menu.close.bind(menu));
};

window.fn.switchLang = function()
{
	langIdx++
	
	if ( langIdx > langs.length - 1 )
	{
		langIdx = 0
	}
	
	var htmlTag = document.getElementsByTagName("html")[0]
	htmlTag.setAttribute("lang", langs[langIdx])
	
	var langBtn = document.getElementById("langBtn")
	langBtn.querySelector("img").setAttribute("src", "/img/"+langs[langIdx]+".png")
	
}

document.addEventListener('DOMContentLoaded', function()
{
  
  ons.ready(function()
  {
	  console.log("ons ready")
  })
  
})
